@echo off

set MSYS2=msys2-base-x86_64-20210604
set EMACS_VER=emacs-27.2
set PHP_VER=php-8.0.6
set NODEJS_NUMVER=16.3.0
set NODEJS_VER=nodejs-%NODEJS_NUMVER%

set DEST=%LOCALAPPDATA%\Emacs
set INSTALLER=%cd%
set HOME=%DEST%\home
set LC_ALL=en_US.UTF-8
set LC_CTYPE=en_US.UTF-8
set LANG=en_US.UTF-8

%HOMEDRIVE%
mkdir %DEST%\home
mkdir %DEST%\%EMACS_VER%
mkdir %DEST%\%PHP_VER%
mkdir %DEST%\tmp
cd %DEST%\

rem Retreive everything
cd %DEST%\tmp
%INSTALLER%\bin\curl -L http://repo.msys2.org/distrib/x86_64/%MSYS2%.tar.xz -o %MSYS2%.tar.xz
%INSTALLER%\bin\curl -L http://ftp.gnu.org/gnu/emacs/windows/emacs-27/%EMACS_VER%-x86_64.zip -o %EMACS_VER%.zip
%INSTALLER%\bin\curl -L https://windows.php.net/downloads/releases/archives/%PHP_VER%-nts-Win32-vs16-x64.zip -o %PHP_VER%.zip
%INSTALLER%\bin\curl -L https://nodejs.org/dist/v%NODEJS_NUMVER%/node-v%NODEJS_NUMVER%-win-x64.zip -o %NODEJS_VER%.zip

rem mingw64
cd %DEST%\tmp
%INSTALLER%\bin\7za x %MSYS2%.tar.xz
cd %DEST%
%INSTALLER%\bin\7za x %DEST%\tmp\%MSYS2%.tar
rem emacs
cd %DEST%\%EMACS_VER%
%INSTALLER%\bin\7za x -aoa %DEST%\tmp\%EMACS_VER%.zip
rem php
cd %DEST%\%PHP_VER%
%INSTALLER%\bin\7za x -aos %DEST%\tmp\%PHP_VER%.zip
rem nodejs
cd %DEST%
%INSTALLER%\bin\7za x -aos %DEST%\tmp\%NODEJS_VER%.zip
ren node-v%NODEJS_NUMVER%-win-x64 %NODEJS_VER%

rem set path
set PATH=%DEST%\msys64\usr\bin;%DEST%\msys64\mingw64\bin;%DEST%\%EMACS_VER%\bin;%DEST%\%PHP_VER%;%DEST%\%NODEJS_VER%;%PATH%

rem configure mingw64
cd %DEST%\msys64
dash /usr/bin/rebaseall -p
echo "Configure mingw64 ..."
cmd /c msys2_shell.cmd -mingw64 -no-start -here -c "cmd /c 'waitfor /si EmacsInstall'"
waitfor EmacsInstall
echo "Upgrade msys2 ..."
cmd /c msys2_shell.cmd -here -no-start -c "pacman --noconfirm -Syuu; cmd /c 'waitfor /si EmacsInstall'"
waitfor EmacsInstall
taskkill /IM dirmngr.exe /F
taskkill /IM gpg-agent.exe /F
pacman --noconfirm -Syuu
rem install needed software
pacman --noconfirm -Sy tar git diffutils svn mingw64/mingw-w64-x86_64-lua mingw64/mingw-w64-x86_64-discount

rem config php
cp %DEST%\%PHP_VER%\php.ini-development %DEST%\%PHP_VER%\php.ini
sed -i 's#;extension_dir.*#extension_dir = "ext"#' %DEST%\%PHP_VER%\php.ini
sed -i 's#;extension=openssl#extension=openssl#' %DEST%\%PHP_VER%\php.ini
cd %DEST%\tmp
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar %DEST%\%PHP_VER%\
echo @php "%DEST%\%PHP_VER%\composer.phar" %%*>%DEST%\%PHP_VER%\composer.bat

rem configure nodejs
cd %DEST%\%NODEJS_VER%
bash npm install --prefix %DEST%\%NODEJS_VER% -g init -y
bash npm install --prefix %DEST%\%NODEJS_VER% -g tern
bash npm install --prefix %DEST%\%NODEJS_VER% -g js-beautify
bash npm install --prefix %DEST%\%NODEJS_VER% -g eslint

rem spacemacs
git clone https://github.com/syl20bnr/spacemacs.git %HOME%\.emacs.d
copy %INSTALLER%\config\.spacemacs %HOME%

rem emacs.cmd
cp %INSTALLER%\cmd\emacs.cmd %DEST%
sed -i 's#_EMACS_#%EMACS_VER%#g' %DEST%\emacs.cmd
sed -i 's#_PHP_VER_#%PHP_VER%#g' %DEST%\emacs.cmd
sed -i 's#_NODEJS_VER_#%NODEJS_VER%#g' %DEST%\emacs.cmd

%INSTALLER%\bin\Shortcut /f:"%AppData%\Microsoft\Windows\Start Menu\Programs\Emacs.lnk" /t:%DEST%\emacs.cmd /a:c /i:%DEST%\%EMACS_VER%\bin\emacs.exe /w:%DEST%


rem patch for bad elap key
sed -i '1s/^/(setq package-check-signature nil)\n/' %HOME%\.emacs.d\init.el

rem menage
rmdir /s /q %DEST%\tmp
