# .(dot)spacemacs

- For 4K display use:

```lisp
  default-font '("Source Code Pro"
                                 :size 22
                                 :weight normal
                                 :width normal
                                 :powerline-scale 1.1
```

