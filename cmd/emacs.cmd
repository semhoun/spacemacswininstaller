@echo off
set PREFIX=%LOCALAPPDATA%\Emacs
set EMACS_VER=_EMACS_
set PHP_VER=_PHP_VER_
set NODEJS_VER=_NODEJS_VER_
set HOME=%PREFIX%\home
set PATH=%PREFIX%\%EMACS_VER%\bin;%PREFIX%\msys64\usr\bin;%PREFIX%\msys64\mingw64\bin;%PREFIX%\%PHP_VER%;%PREFIX%\%NODEJS_VER%;%PATH%
set LC_ALL=en_US.UTF-8
set LC_CTYPE=en_US.UTF-8
set LANG=en_US.UTF-8
emacsclientw.exe --alternate-editor="runemacs.exe" ""
